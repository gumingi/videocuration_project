# buzz & beyond

## 1. 기술 스택

### 1.1. Front

- graphql apollo
- redux
- react

### 1.2. Back

## 2. 초기세팅

### 2.1. Front

- npm install list

  - prettier
  - eslint-config-prettier
  - eslint-plugin-prettier
  - react-router-dom
  - @apollo/client
  - react-youtube

### 2.2. Back

- npm install list

### 2.3. .env 세팅

REACT_APP_EC2_SERVER=""
REACT_APP_LOCAL_SERVER=""

### 2.4. 공통 convention

#### 2.4.1 git

#### 2.4.2. branch

- front: fe/{}/
- back: be/{}/

#### 2.4.3. commit message
